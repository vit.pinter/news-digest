import requests
import os
from send_email import sendEmail
from datetime import datetime, timedelta


yesterday = datetime.today() -timedelta(days=1)
yesterday = yesterday.strftime("%Y-%m-%d")

api_key = os.getenv("NEWSAPI")
topic = "九州"
language = "jp"
url = "https://newsapi.org/v2/everything?" \
      "q=" + topic + "&" \
      "language=" + language + "&" \
      "from=" + yesterday + "" \
      "&sortBy=publishedAt&" \
      "apiKey=" + api_key

# Make request
request = requests.get(url)

# Get a dictionary
content = request.json()
message = "Subject: Today's news\n"

for article in content["articles"][:20]:
    if article["title"] is not None:

        message += article["title"] + '\n'
        message += article["description"] + '\n'
        message += article["url"] + '\n'
        message += '\n\n'

sendEmail(message)
